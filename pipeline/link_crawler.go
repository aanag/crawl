package pipeline

import (
	"context"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

var (
	exclusionRegex = regexp.MustCompile(`(?i)\.(?:jpg|jpeg|png|gif|ico|css|js)$`)
)

// LinkCrawler implements a processor that crawls HTML pages and extracts all
// relative/absolute URLs to a particular domain. The crawler respects HTML
// <base> tags and collects links from href/src attributes.
type LinkCrawler struct {
	domain string
	client *http.Client

	// A regexp to match the <base> tag. If present, it specifies the
	// relative path for links.
	baseRegex *regexp.Regexp

	// A regexp to match absolute and relative paths in href/src elements
	pathRegex *regexp.Regexp
}

// NewLinkCrawler creates a new LinkCrawler instance that uses the provided
// only crawls links http.Client to crawl links that belong to the specified
// domain name.
func NewLinkCrawler(domain string, client *http.Client) *LinkCrawler {
	return &LinkCrawler{
		domain:    domain,
		client:    client,
		baseRegex: regexp.MustCompile(`(?i)<base.*?href\s*?=\s*?"(.*?)\s*?"`),
		pathRegex: regexp.MustCompile(`(?i)<(?:img|image|a|script).*?(?:href|src)\s*?=\s*?"\s*?(.*?)\s*?"`),
	}
}

// Process implements Processor.
func (lc *LinkCrawler) Process(ctx context.Context, inCh <-chan *Job, outCh chan<- *Job, errCh chan<- error) {
	for {
		select {
		case <-ctx.Done():
			return
		case job := <-inCh:
			// Check for known blacklisted extensions and bypass fetching
			// since they don't contain any html content anyway
			if !exclusionRegex.MatchString(job.URL) {
				res, err := lc.client.Get(job.URL)
				if err != nil {
					errCh <- err
					continue
				}

				// Only crawl html documents
				if contentType := res.Header.Get("Content-Type"); res.StatusCode == http.StatusOK && strings.Contains(contentType, "html") {
					content, err := ioutil.ReadAll(res.Body)
					_ = res.Body.Close()
					if err != nil {
						errCh <- err
						continue
					}

					lc.extractLinks(content, job)
				} else {
					_, _ = io.Copy(ioutil.Discard, res.Body)
					_ = res.Body.Close()
				}
			}

			// Push job to next stage
			select {
			case outCh <- job:
			case <-ctx.Done():
				return
			}
		}
	}
}

func (lc *LinkCrawler) extractLinks(content []byte, job *Job) {
	job.URL = ensureHasTrailingSlash(job.URL)
	relBase, _ := url.Parse(job.URL)

	// Look for a base tag which overrides relBase
	if baseMatch := lc.baseRegex.FindSubmatch(content); len(baseMatch) == 2 {
		if baseOverride := resolveURL(relBase, ensureHasTrailingSlashBytes(baseMatch[1])); baseOverride != nil {
			relBase = baseOverride
		}
	}

	// Process absolute/relative links
	for _, match := range lc.pathRegex.FindAllSubmatch(content, -1) {
		// resolve URL and strip out any anchors; ignore URLs that point
		// to different domains and/or have non http(s) schemes
		resolved := resolveURL(relBase, match[1])
		if resolved == nil || resolved.Host != relBase.Host || (resolved.Scheme != "http" && resolved.Scheme != "https") {
			continue
		}

		resolved.Fragment = ""
		job.Links = append(job.Links, resolved.String())
	}
}

// resolveURL expands target into an absolute URL using the following rules:
// - targets starting with '//' are treated as absolute URLs that inherit the
//   protocol from relBase.
// - targets starting with '/' are concatenated with the relBase (scheme:host)
// - everything else is assumed to be relative to relBase.
//
// If the target URL cannot be parsed, an error will be returned
func resolveURL(relBase *url.URL, target []byte) *url.URL {
	tLen := len(target)
	if tLen == 0 {
		return nil
	}

	if tLen >= 1 && target[0] == '/' {
		if tLen >= 2 && target[1] == '/' {
			target = append(
				append([]byte(relBase.Scheme), ':'),
				target...,
			)
		}

		absURL, err := url.Parse(string(target))
		if err != nil {
			return nil
		}

		return relBase.ResolveReference(absURL)
	}

	relURL, err := url.Parse(string(target))
	if err != nil {
		return nil
	}

	return relBase.ResolveReference(relURL)
}

func ensureHasTrailingSlash(s string) string {
	if s[len(s)-1] != '/' {
		return s + "/"
	}
	return s
}

func ensureHasTrailingSlashBytes(s []byte) []byte {
	if s[len(s)-1] != '/' {
		return append(s, '/')
	}
	return s
}
