package pipeline

import (
	"context"
	"errors"
	"fmt"
	"sync"

	multierror "github.com/hashicorp/go-multierror"
)

var (
	errNilProcessor     = errors.New("nil processor")
	errInvalidProcCount = errors.New("processor count must be >=1")
)

// Job encapsulates the required information for a crawler job.
type Job struct {
	// The URL to crawl.
	URL string

	// The list of links referenced by the crawled URL.
	Links []string
}

// Processor is a function that reads Jobs off inCh, applies a processing
// function f to each item and either writes it back to outCh or emits an
// error to errCh.
type Processor func(ctx context.Context, inCh <-chan *Job, outCh chan<- *Job, errCh chan<- error)

// Stage describes a pipeline stage.
type Stage struct {
	// Processor is a function that processes incoming jobs.
	Processor Processor

	// NumProcessors specifies the number of concurrent processors that
	// the pipeline will spin up for this stage. Jobs will be automatically
	// fanned-out to all available processors.
	NumProcessors int
}

func (s Stage) validate() error {
	if s.Processor == nil {
		return errNilProcessor
	} else if s.NumProcessors <= 0 {
		return errInvalidProcCount
	}

	return nil
}

// Pipeline applies a sequence of transformation stages to a batch of Jobs.
type Pipeline struct {
	ctx      context.Context
	cancelFn func()

	inCh  chan *Job
	errCh chan error

	stageOutCh []chan *Job
	wg         sync.WaitGroup
}

// New creates a new pipeline with the specified stages.
func New(ctx context.Context, stages ...Stage) (*Pipeline, error) {
	var (
		err           error
		numProcessors int
	)

	for stageIndex, stage := range stages {
		if valErr := stage.validate(); valErr != nil {
			err = multierror.Append(err, fmt.Errorf("stage %d: %v", stageIndex, valErr))
			continue
		}
		numProcessors += stage.NumProcessors
	}

	if err != nil {
		return nil, err
	}

	pCtx, pCancel := context.WithCancel(ctx)
	p := &Pipeline{
		ctx:      pCtx,
		cancelFn: pCancel,
		inCh:     make(chan *Job),
		errCh:    make(chan error, numProcessors),
	}
	p.spawnProcessors(stages)
	return p, nil
}

func (p *Pipeline) spawnProcessors(stages []Stage) {
	p.stageOutCh = make([]chan *Job, len(stages))

	for stageIndex, stage := range stages {
		p.stageOutCh[stageIndex] = make(chan *Job)

		var stageInCh chan *Job
		if stageIndex == 0 {
			stageInCh = p.inCh
		} else {
			stageInCh = p.stageOutCh[stageIndex-1]
		}

		for worker := 0; worker < stage.NumProcessors; worker++ {
			p.wg.Add(1)
			go func(stage Stage, stageInCh <-chan *Job, stageOutCh chan<- *Job, errCh chan<- error) {
				defer p.wg.Done()
				stage.Processor(p.ctx, stageInCh, stageOutCh, p.errCh)
			}(stage, stageInCh, p.stageOutCh[stageIndex], p.errCh)
		}
	}
}

// Close is a blocking call that shuts down all active processors, drains and
// returns any pending errors.
func (p *Pipeline) Close() error {
	p.cancelFn()
	p.wg.Wait()

	// Drain any errors
	var err error
	close(p.errCh)
	for pErr := range p.errCh {
		err = multierror.Append(err, pErr)
	}
	return err
}

// Process a Job batch and return any errors. This call blocks until all jobs
// have been processed through the pipeline or the context that got passed to
// New gets cancelled.
func (p *Pipeline) Process(jobs []*Job) error {
	pendingJobs := len(jobs)
	go func() {
		for _, job := range jobs {
			select {
			case p.inCh <- job:
			case <-p.ctx.Done():
				return
			}
		}
	}()

	// Wait for all jobs to go through
	var err error
	outCh := p.stageOutCh[len(p.stageOutCh)-1]
	for pendingJobs != 0 {
		select {
		case <-p.ctx.Done():
			err = multierror.Append(err, p.ctx.Err())
			return err
		case jobErr := <-p.errCh:
			pendingJobs--
			err = multierror.Append(err, jobErr)
		case <-outCh:
			pendingJobs--
		}
	}

	return err
}
