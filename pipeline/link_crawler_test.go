package pipeline

import (
	"net/http"
	"net/url"
	"reflect"
	"testing"
)

func TestLinkExtraction(t *testing.T) {
	specs := []struct {
		jobURL   string
		payload  string
		expLinks []string
	}{
		// Rel path to job URL
		{
			jobURL:  "http://example.com",
			payload: `<a href="somewhere"> <A hReF="somewhere/else"> `,
			expLinks: []string{
				"http://example.com/somewhere",
				"http://example.com/somewhere/else",
			},
		},
		// Base tag with absolute href + rel path
		{
			jobURL: "http://example.com/",
			payload: `
<html>
<head><base href="http://example.com/secret"/></head>
<a href="somewhere"> `,
			expLinks: []string{
				"http://example.com/secret/somewhere",
			},
		},
		// Base tag with relative href + rel path
		{
			jobURL: "http://example.com/",
			payload: `
<html>
<head><base href="/account/payments/"/></head>
<a href="somewhere"> `,
			expLinks: []string{
				"http://example.com/account/payments/somewhere",
			},
		},
		// Absolute path containing the domain and a non-related domain
		{
			jobURL: "http://example.com/",
			payload: `
<img src="http://example.com/some/path"> 
<script src="https://example.com/some/path/foo.js"> 
<script src="https://evil.com/some/path/foo.js"> 
`,
			expLinks: []string{
				"http://example.com/some/path",
				"https://example.com/some/path/foo.js",
			},
		},
		// Links with fragments
		{
			jobURL: "http://example.com/",
			payload: `
<a href="/faq#some-anchor"/>
<a href="/faq#some-other-anchor"/>`,
			expLinks: []string{
				"http://example.com/faq",
				"http://example.com/faq",
			},
		},
		// Links that inherit the protocol from the crawled page
		{
			jobURL:  "https://example.com/",
			payload: `<a href="//example.com/payment"/>`,
			expLinks: []string{
				"https://example.com/payment",
			},
		},
		// Malformed URLs
		{
			jobURL:  "http://example.com/",
			payload: `<img src="ht://tp.example.com/some/path">`,
		},
	}

	lc := NewLinkCrawler("example.com", http.DefaultClient)
	for specIndex, spec := range specs {
		job := &Job{URL: spec.jobURL}
		lc.extractLinks([]byte(spec.payload), job)

		if !reflect.DeepEqual(job.Links, spec.expLinks) {
			t.Errorf("[spec %d] expected links:\n%#+v\ngot:\n%#+v", specIndex, spec.expLinks, job.Links)
		}
	}
}

func TestResolveURL(t *testing.T) {
	specs := []struct {
		relBase string
		target  string
		expURL  string
	}{
		{
			relBase: "http://example.com/foo/",
			target:  "/bar/baz",
			expURL:  "http://example.com/bar/baz",
		},
		{
			relBase: "http://example.com/foo/",
			target:  "bar/baz",
			expURL:  "http://example.com/foo/bar/baz",
		},
		{
			relBase: "http://example.com/foo/secret/",
			target:  "./bar/baz",
			expURL:  "http://example.com/foo/secret/bar/baz",
		},
		{
			relBase: "http://example.com/foo/secret/",
			target:  "../../bar/baz",
			expURL:  "http://example.com/bar/baz",
		},
		{
			relBase: "https://example.com/foo/secret/",
			target:  "//www.somewhere.com/foo",
			expURL:  "https://www.somewhere.com/foo",
		},
	}

	for specIndex, spec := range specs {
		relBase, _ := url.Parse(spec.relBase)
		var gotURL string
		if got := resolveURL(relBase, []byte(spec.target)); got != nil {
			gotURL = got.String()
		}

		if gotURL != spec.expURL {
			t.Errorf("[spec %d] expected to get %q; got %q", specIndex, spec.expURL, gotURL)
		}
	}
}
