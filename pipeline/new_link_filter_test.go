package pipeline

import (
	"reflect"
	"testing"
)

func TestNewLinkFilter(t *testing.T) {
	lf := NewLinkFilter()
	job := &Job{
		Links: []string{
			"http://example.com/page1",
			// Same link but with https
			"https://example.com/page1",
			// Same link as before
			"http://example.com/page1",
			// A new link
			"http://example.com/page2",
			// Same link with trailing slash
			"http://example.com/page2/",
		},
	}

	lf.retainNonSeenLinks(job)
	exp := []string{
		"http://example.com/page1",
		"http://example.com/page2",
	}
	if !reflect.DeepEqual(job.Links, exp) {
		t.Fatalf("expected retained links to be:\n%#+v\ngot:\n%#+v", exp, job.Links)
	}
}
