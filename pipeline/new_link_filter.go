package pipeline

import (
	"context"
	"regexp"
	"strings"
)

var schemeRegex = regexp.MustCompile("^[a-zA-Z0-9]+://")

// LinkFilter is a processor that removes any already seen links for incoming
// Job entries.
type LinkFilter struct {
	seen map[string]struct{}
}

// NewLinkFilter creates a new LinkFilter instance.
func NewLinkFilter() *LinkFilter {
	return &LinkFilter{
		seen: make(map[string]struct{}),
	}
}

// Process implements the Processor type.
func (lf *LinkFilter) Process(ctx context.Context, inCh <-chan *Job, outCh chan<- *Job, _ chan<- error) {
	for {
		select {
		case <-ctx.Done():
			return
		case job := <-inCh:
			lf.retainNonSeenLinks(job)

			// Push job to next stage
			select {
			case outCh <- job:
			case <-ctx.Done():
				return
			}
		}
	}
}

func (lf *LinkFilter) retainNonSeenLinks(job *Job) {
	// Iterate each link, strip its scheme and trailing slash and filter it
	// out if we have seen it before
	var retained []string
	for _, link := range job.Links {
		strippedLink := strings.TrimSuffix(schemeRegex.ReplaceAllString(link, ""), "/")
		if _, seen := lf.seen[strippedLink]; !seen {
			lf.seen[strippedLink] = struct{}{}
			retained = append(retained, link)
		}
	}

	job.Links = retained
}
