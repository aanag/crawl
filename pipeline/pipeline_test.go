package pipeline

import (
	"context"
	"errors"
	"sync"
	"sync/atomic"
	"testing"

	multierror "github.com/hashicorp/go-multierror"
)

func TestStageValidator(t *testing.T) {
	var mockProc = func(_ context.Context, _ <-chan *Job, _ chan<- *Job, _ chan<- error) {}

	specs := []struct {
		s      Stage
		expErr error
	}{
		{
			s:      Stage{Processor: nil, NumProcessors: 1},
			expErr: errNilProcessor,
		},
		{
			s:      Stage{Processor: mockProc, NumProcessors: 0},
			expErr: errInvalidProcCount,
		},
		{
			s:      Stage{Processor: mockProc, NumProcessors: 1},
			expErr: nil,
		},
	}

	for specIndex, spec := range specs {
		err := spec.s.validate()
		if (spec.expErr == nil && err != nil) || (spec.expErr != nil && (err == nil || spec.expErr.Error() != err.Error())) {
			t.Errorf("[spec %d] expected validate to return: %v; got %v", specIndex, spec.expErr, err)
		}
	}
}

func TestPipeline(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		var (
			wg            sync.WaitGroup
			mockProcCount [2]int32
			makeProc      = func(targetIndex int) Processor {
				return func(ctx context.Context, inCh <-chan *Job, outCh chan<- *Job, _ chan<- error) {
					defer wg.Done()
					for {
						select {
						case <-ctx.Done():
							return
						case job := <-inCh:
							atomic.AddInt32(&mockProcCount[targetIndex], 1)
							outCh <- job
						}
					}
				}
			}
		)

		wg.Add(3)
		p, err := New(
			context.TODO(),
			Stage{Processor: makeProc(0), NumProcessors: 2},
			Stage{Processor: makeProc(1), NumProcessors: 1},
		)
		if err != nil {
			t.Fatal(err)
		}

		data := []*Job{
			{URL: "foo"},
			{URL: "bar"},
			{URL: "baz"},
		}

		if err = p.Process(data); err != nil {
			t.Fatal(err)
		}

		expCount := int32(len(data))
		for index, got := range mockProcCount {
			if got != expCount {
				t.Errorf("expected mock processor %d to process %d jobs; got %d", index, expCount, got)
			}
		}

		if err = p.Close(); err != nil {
			t.Fatal(err)
		}

		// Ensure that processors cleanly shut down
		wg.Wait()
	})

	t.Run("stage reports error", func(t *testing.T) {
		var (
			mockProcCount int
			mockProc      = func(ctx context.Context, inCh <-chan *Job, _ chan<- *Job, errCh chan<- error) {
				for {
					select {
					case <-ctx.Done():
						return
					case <-inCh:
						mockProcCount++
						errCh <- errors.New("invalid data")
					}
				}
			}
		)

		p, err := New(
			context.TODO(),
			Stage{Processor: mockProc, NumProcessors: 1},
		)
		if err != nil {
			t.Fatal(err)
		}

		data := []*Job{
			{URL: "foo"},
			{URL: "bar"},
			{URL: "baz"},
		}

		if err = p.Process(data); err == nil {
			t.Fatal("expected an error to occur")
		}

		mErr, ok := err.(*multierror.Error)
		if !ok {
			t.Fatal("expected error to be a multierror")
		}
		if exp, got := len(data), len(mErr.Errors); exp != got {
			t.Fatalf("expected to receive %d wrapped errors; got %d", exp, got)
		}

		if err = p.Close(); err != nil {
			t.Fatal(err)
		}
	})
}
