package pipeline

import (
	"context"

	"bitbucket.org/aanag/crawl/graph"
)

// GraphUpdater is a processor that populates a link graph with the set of
// links contained within a processed Job object.
type GraphUpdater struct {
	g *graph.Graph
}

// NewGraphUpdater returns a new GraphUpdater instance that populates g with
// all discovered links.
func NewGraphUpdater(g *graph.Graph) *GraphUpdater {
	return &GraphUpdater{
		g: g,
	}
}

// Process implements Processor.
func (gu *GraphUpdater) Process(ctx context.Context, inCh <-chan *Job, outCh chan<- *Job, _ chan<- error) {
	for {
		select {
		case <-ctx.Done():
			return
		case job := <-inCh:
			for _, dstLink := range job.Links {
				gu.g.AddEdge(job.URL, dstLink)
			}

			// Push job to next stage
			select {
			case outCh <- job:
			case <-ctx.Done():
				return
			}
		}
	}
}
