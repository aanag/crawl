package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/aanag/crawl/graph"
	"bitbucket.org/aanag/crawl/graph/vis"
	"bitbucket.org/aanag/crawl/pipeline"
	"github.com/urfave/cli"
)

func main() {
	app := cli.App{
		Name:      "crawler",
		Version:   "0.0.1",
		Usage:     "crawl all pages belonging to a domain and prints out a link map showing how pages are linked together",
		ArgsUsage: "start-URL",
		Action:    runCrawler,
		Writer:    os.Stderr,
		Flags: []cli.Flag{
			cli.IntFlag{
				Name:  "workers-per-core",
				Usage: "number of crawl workers per CPU core",
				Value: 32,
			},
			cli.StringFlag{
				Name:  "output, o",
				Usage: "the format to output the link map (table, dot)",
				Value: "table",
			},
			cli.BoolFlag{
				Name:  "verbose",
				Usage: "print out a list of discovered links",
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}

func runCrawler(cliCtx *cli.Context) error {
	switch cliCtx.String("output") {
	case "table", "dot":
		// OK
	default:
		return fmt.Errorf("unsupported output type: %s", cliCtx.String("output"))
	}

	ctx, cancelFn := context.WithCancel(context.Background())
	go func() {
		sigCh := make(chan os.Signal)
		signal.Notify(sigCh, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)
		s := <-sigCh
		_, _ = fmt.Fprintf(os.Stderr, "caught signal %s; shutting down", s.String())
		cancelFn()
	}()

	startURL, err := url.Parse(cliCtx.Args().First())
	if err != nil || (startURL.Scheme != "http" && startURL.Scheme != "https") || startURL.Host == "" {
		return errors.New("please specify a URL to crawl (e.g. https://www.example.com)")
	}

	g := graph.New()

	p, err := pipeline.New(
		ctx,
		pipeline.Stage{
			Processor:     pipeline.NewLinkCrawler(startURL.Host, new(http.Client)).Process,
			NumProcessors: runtime.NumCPU() * cliCtx.Int("workers-per-core"),
		},
		pipeline.Stage{
			Processor:     pipeline.NewGraphUpdater(g).Process,
			NumProcessors: 1,
		},
		pipeline.Stage{
			Processor:     pipeline.NewLinkFilter().Process,
			NumProcessors: 1,
		},
	)
	if err != nil {
		return err
	}
	defer func() { _ = p.Close() }()

	// Seed the pipeline with the first URL
	var (
		input = []*pipeline.Job{
			{URL: startURL.String()},
		}
		verboseMode = cliCtx.Bool("verbose")
		numCrawled  int
		start       = time.Now()
	)
	for {
		if err = p.Process(input); err != nil {
			return err
		}

		// Scan the transformed input and generate a new input list
		// that contains a job for each newly discovered link.
		var newJobsList []*pipeline.Job
		for _, job := range input {
			numCrawled += len(job.Links)
			for _, discoveredLink := range job.Links {
				if verboseMode {
					_, _ = fmt.Fprintf(os.Stderr, "[NEW] %s\n", discoveredLink)
				}
				newJobsList = append(newJobsList, &pipeline.Job{URL: discoveredLink})
			}
		}

		// Run out of links to crawl
		if len(newJobsList) == 0 {
			break
		}

		input = newJobsList
	}

	switch cliCtx.String("output") {
	case "table":
		vis.Tabularize(os.Stdout, g)
	case "dot":
		vis.ToDot(os.Stdout, g)
	}

	_, _ = fmt.Fprintf(os.Stderr, "[DONE] crawled %d pages in %s\n", numCrawled, time.Since(start).String())
	return nil
}
