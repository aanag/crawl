# Web page crawler
[![CircleCI](https://circleci.com/bb/aanag/crawl.svg?style=svg)](https://circleci.com/bb/aanag/crawl)
[![codecov](https://codecov.io/bb/aanag/crawl/branch/master/graph/badge.svg)](https://codecov.io/bb/aanag/crawl)

A multi=threaded web page crawler that recursively scans a web page and any
links to the same domain and emits a map of all links.

**Features**:
- crawls and merges both http and https URLs
- handles both absolute and relative links
- detects and processes `<base>` html tags
- support protocol-less links (e.g `<a href="//www.example.com/foo">`)
- emits tabular (ASCII) and graphviz output

**TODO(s)**:
- implement a strategy for handling rate-limit HTTP status codes (e.g 429s)

## Getting started

You can install the binary by typing `make install`. Here is a command to get
you started:

```console 
crawler@localhost$ crawler https://play.golang.org

+------------------------------------------------+----------------+------------------------------------------------+
|                     SOURCE                     | OUTGOING LINKS |                  DESTINATION                   |
+------------------------------------------------+----------------+------------------------------------------------+
| play.golang.org                                |              5 |                                                |
+------------------------------------------------+----------------+------------------------------------------------+
|                                                |                | play.golang.org/p/1VcPUlPk_3                   |
+------------------------------------------------+----------------+------------------------------------------------+
|                                                |                | play.golang.org/playground.js                  |
+------------------------------------------------+----------------+------------------------------------------------+
|                                                |                | play.golang.org/static/gopher.png              |
+------------------------------------------------+----------------+------------------------------------------------+
|                                                |                | play.golang.org/static/jquery-linedtextarea.js |
+------------------------------------------------+----------------+------------------------------------------------+
|                                                |                | play.golang.org/static/playground-embed.js     |
+------------------------------------------------+----------------+------------------------------------------------+
| play.golang.org/static/jquery-linedtextarea.js |              0 |                                                |
+------------------------------------------------+----------------+------------------------------------------------+
| play.golang.org/playground.js                  |              0 |                                                |
+------------------------------------------------+----------------+------------------------------------------------+
| play.golang.org/static/playground-embed.js     |              0 |                                                |
+------------------------------------------------+----------------+------------------------------------------------+
| play.golang.org/static/gopher.png              |              0 |                                                |
+------------------------------------------------+----------------+------------------------------------------------+
| play.golang.org/p/1VcPUlPk_3                   |              4 |                                                |
+------------------------------------------------+----------------+------------------------------------------------+
|                                                |                | play.golang.org/playground.js                  |
+------------------------------------------------+----------------+------------------------------------------------+
|                                                |                | play.golang.org/static/gopher.png              |
+------------------------------------------------+----------------+------------------------------------------------+
|                                                |                | play.golang.org/static/jquery-linedtextarea.js |
+------------------------------------------------+----------------+------------------------------------------------+
|                                                |                | play.golang.org/static/playground-embed.js     |
+------------------------------------------------+----------------+------------------------------------------------+
[DONE] crawled 5 pages in 929.166798ms
```

The crawler also supports graphviz (via the `-o dot` flag) output:

```console 
# note: linux users should use xdg-open instead of open
crawler@localhost$ crawler -o dot https://play.golang.org | dot -T svg -o out.svg && open out.svg

[DONE] crawled 5 pages in 1.021096449s
```

![crawler output](https://drive.google.com/uc?export=download&id=1u8Or0swkVxBa1fVf111C5qfBlYXRHq85)

The crawler supports additional options that can further control its operation.
You can see these options and their default values by accessing the crawler's
help screen:

```console
crawler@localhost$ crawler -h

NAME:
   crawler - crawl all pages belonging to a domain and prints out a link map showing how pages are linked together

USAGE:
    [global options] command [command options] start-URL

VERSION:
   0.0.1

COMMANDS:
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --workers-per-core value  number of crawl workers per CPU core (default: 32)
   --output value, -o value  the format to output the link map (table, dot) (default: "table")
   --verbose                 print out a list of discovered links
   --help, -h                show help
   --version, -v             print the version
```


