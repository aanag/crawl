package graph

import (
	"reflect"
	"testing"
)

func TestGraphVisitor(t *testing.T) {
	g := New()
	g.AddEdge("http://foo.com/a", "http://foo.com/a") // circular links should be dropped
	// Adding the same edge more than once should be a no-op
	g.AddEdge("http://foo.com/a", "http://foo.com/b")
	g.AddEdge("http://foo.com/a", "http://foo.com/b")
	g.AddEdge("http://foo.com/a", "http://foo.com/b")
	//
	g.AddEdge("https://foo.com/a/", "http://foo.com/c")
	g.AddEdge("http://foo.com/b", "http://foo.com/c")

	expLinks := []Link{
		{URL: "foo.com/a", id: 0, edges: []int{1, 2}},
		{URL: "foo.com/b", id: 1, edges: []int{2}},
		{URL: "foo.com/c", id: 2},
	}

	var visitedLinks []Link
	g.Visit(func(l *Link) {
		clone := *l
		clone.edgeMap = nil
		visitedLinks = append(visitedLinks, clone)
	})

	if !reflect.DeepEqual(expLinks, visitedLinks) {
		t.Fatalf("expected visited links to be:\n%#+v\ngot:\n%#+v", expLinks, visitedLinks)
	}
}

func TestGraphNeighborVisitor(t *testing.T) {
	g := New()
	g.AddEdge("http://foo.com/a", "http://foo.com/b")
	g.AddEdge("https://foo.com/a/", "http://foo.com/c")
	g.AddEdge("http://foo.com/b", "http://foo.com/c")

	expLinks := []Link{
		{URL: "foo.com/b", id: 1, edges: []int{2}},
		{URL: "foo.com/c", id: 2},
	}

	var visitedLinks []Link
	g.links[0].VisitNeighbors(func(l *Link) {
		clone := *l
		clone.edgeMap = nil
		visitedLinks = append(visitedLinks, clone)
	})

	if !reflect.DeepEqual(expLinks, visitedLinks) {
		t.Fatalf("expected visited neighbor links to be:\n%#+v\ngot:\n%#+v", expLinks, visitedLinks)
	}
}
