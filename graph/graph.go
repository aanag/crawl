package graph

import (
	"regexp"
	"sort"
	"strings"
)

var schemeRegex = regexp.MustCompile("^[a-zA-Z0-9]+://")

// Link contains information about a discovered link within a crawled domain.
type Link struct {
	URL     string
	id      int
	edges   []int
	edgeMap map[int]*Link
}

// ID returns a unique ID associated with this link.
func (l *Link) ID() int { return l.id }

// NumNeighbors returns the number of outgoing edges from this link.
func (l *Link) NumNeighbors() int { return len(l.edges) }

// VisitNeighbors invokes visitorFn for each link in g that this link connects to.
func (l *Link) VisitNeighbors(visitorFn func(*Link)) {
	for _, linkID := range l.edges {
		visitorFn(l.edgeMap[linkID])
	}
}

// SortNeighbors re-arranges the neighbor links so that they are sorted by URL.
func (l *Link) SortNeighbors() {
	sort.Slice(l.edges, func(i, j int) bool {
		return l.edgeMap[l.edges[i]].URL < l.edgeMap[l.edges[j]].URL
	})
}

// A Graph consists of a set of Links and their connections.
type Graph struct {
	nameToLinkMap map[string]*Link
	links         []*Link
}

// New creates a new Graph instance.
func New() *Graph {
	return &Graph{
		nameToLinkMap: make(map[string]*Link),
	}
}

// Visit invokes visitorFn for each link in the graph.
func (g *Graph) Visit(visitorFn func(*Link)) {
	for _, link := range g.links {
		visitorFn(link)
	}
}

// AddEdge connects from and to creating any required links.
func (g *Graph) AddEdge(from, to string) {
	fromLink := g.findOrCreateLink(from)
	toLink := g.findOrCreateLink(to)

	// Skip if fromLink is the same as toLink
	if fromLink.ID() == toLink.ID() {
		return
	}

	if _, exists := fromLink.edgeMap[toLink.id]; exists {
		return
	}

	fromLink.edges = append(fromLink.edges, toLink.id)
	fromLink.edgeMap[toLink.id] = toLink
}

func (g *Graph) findOrCreateLink(to string) *Link {
	to = normalizeLink(to)
	if l, exists := g.nameToLinkMap[to]; exists {
		return l
	}

	link := &Link{
		URL:     to,
		id:      len(g.links),
		edgeMap: make(map[int]*Link),
	}
	g.links = append(g.links, link)
	g.nameToLinkMap[to] = link

	return link
}

func normalizeLink(l string) string {
	return strings.TrimSuffix(schemeRegex.ReplaceAllString(l, ""), "/")
}
