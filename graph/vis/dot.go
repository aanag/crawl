package vis

import (
	"bufio"
	"fmt"
	"io"

	"bitbucket.org/aanag/crawl/graph"
)

// ToDot outputs a graphviz representation of the graph g to io.Writer w.
func ToDot(w io.Writer, g *graph.Graph) {
	bw := bufio.NewWriter(w)
	_, _ = bw.WriteString(`
digraph l {
overlap=false;
bgcolor=transparent;
splines=true;
rankdir=TB;
node [shape=Mrecord, fontsize=12, style=filled, fillcolor=deepskyblue];
`)

	// Emit nodes
	g.Visit(func(link *graph.Link) {
		_, _ = fmt.Fprintf(bw, "n_%d [label=%q];\n", link.ID(), link.URL)
	})

	// Emit links
	g.Visit(func(link *graph.Link) {
		link.VisitNeighbors(func(other *graph.Link) {
			_, _ = fmt.Fprintf(bw, "n_%d -> n_%d;\n", link.ID(), other.ID())
		})
	})

	_, _ = bw.WriteString("}\n")
	_ = bw.Flush()
}
