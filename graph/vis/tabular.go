package vis

import (
	"fmt"
	"io"

	"bitbucket.org/aanag/crawl/graph"
	"github.com/olekukonko/tablewriter"
)

// Tabularize renders an ASCII table representation of the link map.
func Tabularize(w io.Writer, g *graph.Graph) {
	table := tablewriter.NewWriter(w)
	table.SetRowLine(true)

	table.SetHeader([]string{"Source", "Outgoing links", "Destination"})
	g.Visit(func(link *graph.Link) {
		var rows = [][]string{
			{link.URL, fmt.Sprint(link.NumNeighbors()), ""},
		}

		// Sort neighbors by name to make the output neater.
		link.SortNeighbors()

		link.VisitNeighbors(func(other *graph.Link) {
			rows = append(rows, []string{"", "", other.URL})
		})

		table.AppendBulk(rows)
	})

	table.Render()
}
