.PHONY: lint test install

build: 
	@mkdir -p bin
	go build -o bin/crawler cmd/crawler/crawler.go

install: 
	go install cmd/crawler/crawler.go

LINT_FLAGS :=--disable-all --enable=vet --enable=vetshadow --enable=golint --enable=ineffassign --enable=goconst --enable=gofmt --enable=errcheck --enable=goimports
LINTER := gometalinter.v2

$(LINTER):
	go get -u gopkg.in/alecthomas/$(LINTER)
	$(LINTER) --install

lint: $(LINTER)
	$(LINTER) $(LINT_FLAGS) ./...

test: 
	go test -race -cover -v ./...

test-ci: 
	go test -race -coverprofile=coverage.txt -covermode=atomic ./...
